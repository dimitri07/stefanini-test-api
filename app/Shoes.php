<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Shoes extends Model 
{    
    protected $primaryKey = 'shoesId';

    protected $table = 'shoes';    

    protected $fillable = [
        'reference',
        'name',
        'description',
        'quantity',
        'image'
    ];    
    
    protected $hidden = [];

    public function getShoesById(int $shoesId)
    {
        $result = $this->where('shoesId', $shoesId)->get(); 
        return $this->addProperUrlImagesToProduct($result)[0];
    }

    public function getAllShoes()
    {
        $containerShoes = $this
            ->orderBy('created_at', 'desc')
            ->get(); 

        return $this->addProperUrlImagesToProduct($containerShoes);
    }

    public function changeShoesState(int $shoesId, int $state): bool
    {   
        $shoes = $this->find($shoesId);
        if (!isset($shoes) || is_null($shoes))
            return false;

        $shoes->status = $state === 0 ? 'INACTIVE' : 'ACTIVE';
        $shoes->save();
        return true;
    }

    public function addNewShoes(
        string $reference,
        string $name,
        string $description,
        int $quantity,
        $image
        ): bool
    {
        $shoes = new Shoes();
        $shoes->reference = $reference;
        $shoes->name = $name; 
        $shoes->description = $description;
        $shoes->quantity = $quantity;
        $shoes->image = $image; 
        $shoes->status = 'ACTIVE';
        return $shoes->save(); 
    }

    public function updateShoes(
        int $shoesId,
        string $name,
        string $description,
        int $quantity,
        $image,
        string $status): bool
    {
        $shoes = $this->find($shoesId);
        $shoes->name = $name;
        $shoes->description = $description;
        $shoes->quantity = $quantity;
        if (!empty($image)) 
            $shoes->image = $image;

        $shoes->status = $status;
        return $shoes->save();
    }

    public function existsProductReference(string $reference)
    {
        $result = $this->where('reference', $reference)->get(); 
        return !$result->isEmpty();
    }

    public function filterShoes(int $searchCriteria, string $needle)
    {
        $searchField = $searchCriteria === 1 ? 'reference' : 'name';
        $containerShoes = $this->where($searchField, 'like', '%' . $needle . '%')->orderBy('created_at', 'desc')->get();
        return $this->addProperUrlImagesToProduct($containerShoes);
    }

    public function existsShoes(int $shoesId): bool
    {
        $result = $this->where('shoesId', $shoesId)->get();
        return !$result->isEmpty();
        
    }

    private function addProperUrlImagesToProduct($containerShoes)
    {
        foreach ($containerShoes as $key => $shoes) 
            $shoes->image = url('/images') . '/'  .  $shoes->image;

        return $containerShoes;
    }
}
