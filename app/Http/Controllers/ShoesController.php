<?php
namespace App\Http\Controllers;

use App\Shoes;
use Illuminate\Http\Request;

class ShoesController extends Controller
{
    private $model; 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $headers =  $request->headers->all();
        $response = parent::__construct($headers);
        if (!$response) 
        {
            echo sendBasicResponse(402, [], 'Token es incorrecto');
            exit();
        } 
        $this->model = new Shoes();
    }

    public function getShoesById(Request $request)
    {
        $shoesId = (int) $request->input('shoesId');
        if (!$this->model->existsShoes($shoesId))
            return sendBasicResponse(400, [], 'Identificador del producto no existe');
        
        return sendBasicResponse(
            200, 
            $this->model->getShoesById($shoesId),
            'Operación exitosa'
        );
    }

    public function getAllShoes()
    {
        return sendBasicResponse(200, $this->model->getAllShoes(), 'operacion exitosa');
    }

    public function addNewShoes(Request $request)
    {
        if (!$request->hasFile('image')) 
            return sendBasicResponse(400, [], 'Debe adjuntar una imagen del producto');

        if (!$request->file('image')->isValid())
            return sendBasicResponse(400, [], 'Hay un error en la imagen adjuntada, por favor verifique');

        $reference = $request->input('reference');
        $name = $request->input('name');
        $description = $request->input('description');
        $quantity = $request->input('quantity');
        $file = $request->file('image');
        $newFileName = uniqid() . '.' .  $file->extension();
        $request->file('image')->move('images/', $newFileName);

        if ($this->model->existsProductReference($reference))
            return sendBasicResponse(400, [], 'La referencia/código de este producto ya existe'); 

        $savingShoes = $this->model->addNewShoes(
            $reference, 
            $name,
            $description,
            $quantity,
            $newFileName
        );
        return $savingShoes ? 
            sendBasicResponse(200, '', 'Producto agregado correctamente') : 
            sendBasicResponse(300, '', 'Ha ocurrido un error al guardar el producto');
    }

    public function updateShoes(Request $request)
    {
        if (!$request->hasFile('image')) 
            $newFileName = '';
        else
        {
            $file = $request->file('image');
            $newFileName = uniqid() . '.' .  $file->extension();
            $request->file('image')->move('images/', $newFileName);
        } 
        $shoesId = (int)  $request->input('shoesId');
        if (!$this->model->existsShoes($shoesId))
            return sendBasicResponse(400, [], 'Identificador del producto no existe');
        
        $this->model->updateShoes(
            $shoesId,
            $request->input('name'),
            $request->input('description'),
            (int) $request->input('quantity'),
            $newFileName,
            $request->input('status')
        );
        return sendBasicResponse(200, [], 'Operación exitosa');
    }

    /**
     *  searchCriteria = 1 search by shoes reference
     * searchCriteria = 2 search by shoes name
     */
    public function filterShoes(Request $request)
    {
        $searchShoes = $this->model->filterShoes(
            (int) $request->input('searchCriteria'),
            $request->input('needle')
        ); 
        return sendBasicResponse(200, $searchShoes, 'Operación exitosa');
    }
}
