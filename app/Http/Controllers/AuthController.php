<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private $model; 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $headers =  $request->headers->all();
        $response = parent::__construct($headers);
        if (!$response) 
        {
            echo sendBasicResponse(402, [], 'Token es incorrecto');
            exit();
        }
        $this->model = new User();
    }

    public function login(Request $request)
    {
        $login = $this->model->login(
            $request->input('email'),
            $request->input('password')
        );
        return $login['success'] ? 
            sendBasicResponse(200, $login['user'], 'Operación exitosa') :
            sendBasicResponse(400, [], 'Credenciales incorrectas');
    }

    public function createAccount(Request $request)
    {
        $newAccount = $this->model->createAccount(
            $request->input('email'),
            $request->input('fullName'),
            $request->input('password')
        );
    }
}
