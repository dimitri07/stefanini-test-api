<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function __construct(array $headers)
    {   
        $authToken = $headers["x-auth-token"][0];
        return $authToken === AUTH_TOKEN;
    }

}
