<?php

define('AUTH_TOKEN', 'eyJ0eXAiJ9.eyJpYXQiOjE1NjjAsIm5iZiI6MTU2NDQxNjI2MCwiZXhwIjoxNTY0NDE5ODYwLCJkYXRhIjp7InVpZCI6ImwxWTBudHVYS1wvXC9uVkdhbTlTN3JSTkxOSmVMdzRNQ2ttcnh6cmpybjZ3az0ifX0.BKVqPeX74p644Z77MYkHxhwvACHO1vJ9cm59vMjC5eM');

function sendBasicResponse(int $code, $data, string $description): string
{
    return json_encode(
        [
            'code'          => $code,
            'data'          => $data,
            'description'   => $description
        ]
    );
}

function checkIsEmpty($expression): bool
{
    $checkNull = is_null($expression);
    if ($checkNull)
        return $checkNull;
    
    // Array
    if (is_array($expression)) 
        return (!isset($expression) || empty($value) || count($expression) <= 0) || $expression === null;
    
    //string
    if (is_string($expression)) 
        return (!isset($expression) || trim($expression) === '');
    
    // Object and another
    return (!isset($expression)) || empty($expression);
}

function authenticateUser(array $headers)
{

}