<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shoes')->insert([
            'reference' => 'AXDRSSDT',
            'name' => 'Nike total 90 II',
            'description' => 'Zapatos geniales para la práctica del fútbol',
            'quantity' => 200,
            'image' => 'Default_image.jpg',
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
