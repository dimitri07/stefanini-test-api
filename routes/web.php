<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/getShoesList', 'ShoesController@getAllShoes');
$router->post('/addNewShoes', 'ShoesController@addNewShoes');
$router->post('/filterShoes', 'ShoesController@filterShoes');
$router->post('/updateShoes', 'ShoesController@updateShoes');
$router->post('/getShoesById', 'ShoesController@getShoesById');

$router->post('/login', 'AuthController@login');

